# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at
  # http://localhost:3000/rails/mailers/user_mailer/account_activation
  def account_activation
    user = User.first
    user.activation_token = User.new_token
    UserMailer.account_activation(user)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/user_mailer/password_reset
  def password_reset
    user = User.first
    user.reset_token = User.new_token
    UserMailer.password_reset(user)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/user_mailer/trip_booked
  def trip_booked
    user = User.first
    trip = Trip.first
    UserMailer.trip_booked(user,trip)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/user_mailer/trip_canceled
  def trip_canceled
    user = User.first
    trip = Trip.first
    UserMailer.trip_canceled(user,trip)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/user_mailer/trip_deleted
  def trip_deleted
    user = User.first
    trip = Trip.first
    UserMailer.trip_deleted(user,trip)
  end
end