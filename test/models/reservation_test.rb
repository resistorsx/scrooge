require 'test_helper'

class ReservationTest < ActiveSupport::TestCase

  def setup
    @reservation = Reservation.new(trip_id: trips(:orange).id,
                                   passenger_id: users(:archer).id)
  end

  test "should be valid" do
    assert @reservation.valid?
  end

  test "should require a trip_id" do
    @reservation.trip_id = nil
    assert_not @reservation.valid?
  end

  test "should require a passenger_id" do
    @reservation.passenger_id = nil
    assert_not @reservation.valid?
  end
end