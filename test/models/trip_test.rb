require 'test_helper'

class TripTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @trip = @user.trips.build(tripinfo: "Lorem ipsum", tripdate: Time.zone.now,
    									       maxtravellers: 4, triptype: false, user_id: @user.id)
  end

  test "should be valid" do
    assert @trip.valid?
  end

  test "user id should be present" do
    @trip.user_id = nil
    assert_not @trip.valid?
  end

  test "tripinfo should be present" do
    @trip.tripinfo = "   "
    assert_not @trip.valid?
  end

  test "tripinfo should be at most 140 characters" do
    @trip.tripinfo = "a" * 141
    assert_not @trip.valid?
  end

  test "tripdate should be set" do
    @trip.tripdate = nil
    assert_not @trip.valid?
  end

  test "maxtravellers should be not greather than 4" do
    @trip.maxtravellers = 5
    assert_not @trip.valid?
  end

  test "triptype should be set" do
    @trip.triptype = nil
    assert_not @trip.valid?
  end

  test "order should be next trip first" do
    assert_equal trips(:next_trip), Trip.first
  end
end