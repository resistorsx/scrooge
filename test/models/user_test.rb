require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                     travelcoins: 1,
    								 password: "foobar11", password_confirmation: "foobar11")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "travelcoins should be present" do
    @user.travelcoins = nil
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 8
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 7
    assert_not @user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated trips should be destroyed" do
    @user.save
    @user.trips.create!(tripinfo: "Lorem ipsum", tripdate: Time.zone.now, 
                        maxtravellers: 4, triptype: true)
    assert_difference 'Trip.count', -1 do
      @user.destroy
    end
  end

  test "should be friend or not to a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.myfriend?(archer)
    assert_not archer.friendof?(michael)
    michael.myfriend(archer)
    assert michael.myfriend?(archer)
    assert archer.friendof?(michael)
    michael.notfriend(archer)
    assert_not michael.myfriend?(archer)
    assert_not archer.friendof?(michael)
  end

  test "feed should have the right trips" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    publict = trips(:public_trip)
    # Trips from traveler user
    lana.trips.each do |trip_traveler|
      assert michael.feed.include?(trip_traveler)
    end
    # Trips from self
    michael.trips.each do |trip_self|
      assert michael.feed.include?(trip_self)
    end
    # Trips from not friend user
    michael.trips.each do |trip_unfollowed|
      assert_not archer.feed.include?(trip_unfollowed)
    end
    #Public trips
    assert archer.feed.include?(publict)
  end

  test "should book and cancel a reservation" do
    trip =    trips(:orange)
    archer  = users(:archer)
    lana    = users(:lana)
    assert_not trip.booked?(archer)
    assert_difference 'Reservation.count', 1 do
      trip.book(archer)
    end
    assert trip.booked?(archer)
    assert archer.bookings.include?(trip)
    assert_difference 'Reservation.count', -1 do
      trip.cancel(archer)
    end
    assert_not trip.booked?(archer)
    assert_not archer.bookings.include?(trip)
  end

  test "should not book a fully booked trip" do
    trip =    trips(:next_trip)
    archer  = users(:archer)
    lana    = users(:lana)
    assert_not trip.booked?(archer)
    assert_difference 'Reservation.count', 1 do
      trip.book(archer)
    end
    assert trip.booked?(archer)
    assert_no_difference 'Reservation.count' do
      trip.book(lana)
    end
    assert_not trip.booked?(lana)
  end
end