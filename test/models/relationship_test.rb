require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase

   def setup
    @relationship = Relationship.new(driver_id: users(:michael).id,
                                     traveler_id: users(:archer).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a driver_id" do
    @relationship.driver_id = nil
    assert_not @relationship.valid?
  end

  test "should require a traveler_id" do
    @relationship.traveler_id = nil
    assert_not @relationship.valid?
  end
end
