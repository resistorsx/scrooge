require 'test_helper'

class BookingTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @trip = trips(:orange)
    log_in_as(@user)
  end

  test "should book a trip the standard way" do
    assert_difference '@trip.bookings', 1 do
      post reservations_path, params: { trip_id: @trip.id }
    end
  end

  test "should book a trip with Ajax" do
    assert_difference '@trip.bookings', 1 do
      post reservations_path, xhr: true, params: { trip_id: @trip.id }
    end
  end

  test "should cancel a trip the standard way" do
    @trip.book(@user)
    reservation = @trip.active_reservations.find_by(passenger_id: @user.id)
    assert_difference '@trip.bookings', -1 do
      delete reservation_path(reservation)
    end
  end

  test "should cancel a trip with Ajax" do
    @trip.book(@user)
    reservation = @trip.active_reservations.find_by(passenger_id: @user.id)
    assert_difference '@trip.bookings', -1 do
      delete reservation_path(reservation), xhr: true
    end
  end
end