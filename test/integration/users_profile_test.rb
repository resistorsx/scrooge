require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:michael)
  end

  test "profile display" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: "#{@user.name} (#{@user.travelcoins})"
    assert_select 'h1>img.gravatar'
    assert_match @user.trips.count.to_s, response.body
    @user.trips.each do |trip|
      assert_match trip.tripinfo, response.body
    end
  end
end