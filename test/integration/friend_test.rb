require 'test_helper'

class FriendTest < ActionDispatch::IntegrationTest

  def setup
    @user  = users(:michael)
    @other = users(:archer)
    log_in_as(@user)
  end

  test "should follow a user the standard way" do
    assert_difference '@user.travelers.count', 1 do
      post relationships_path, params: { traveler_id: @other.id }
    end
  end

  test "should follow a user with Ajax" do
    assert_difference '@user.travelers.count', 1 do
      post relationships_path, xhr: true, params: { traveler_id: @other.id }
    end
  end

  test "should unfollow a user the standard way" do
    @user.myfriend(@other)
    relationship = @user.active_relationships.find_by(traveler_id: @other.id)
    assert_difference '@user.travelers.count', -1 do
      delete relationship_path(relationship)
    end
  end

  test "should unfollow a user with Ajax" do
    @user.myfriend(@other)
    relationship = @user.active_relationships.find_by(traveler_id: @other.id)
    assert_difference '@user.travelers.count', -1 do
      delete relationship_path(relationship), xhr: true
    end
  end
end