require 'test_helper'

class TripsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "trip interface" do
    log_in_as(@user)
    get root_path
    # Invalid submission
    assert_no_difference 'Trip.count' do
      post trips_path, params: { trip: { tripinfo: "" } }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    tripinfo = "Roma-Paris"
    assert_difference 'Trip.count', 1 do
      post trips_path, params: { trip: { tripinfo: tripinfo,
      																		tripdate: "2016-12-25 07:10:00",
      																		maxtravellers: 4, triptype: true } }
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match tripinfo, response.body
    # Delete post
    assert_select 'a', text: 'delete'
    first_trip = @user.trips.first
    assert_difference 'Trip.count', -1 do
      delete trip_path(first_trip)
    end
    # Visit different user (no delete links)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end

  test "trip sidebar count" do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.trips.count} trips", response.body
    # User with zero trips
    other_user = users(:malory)
    log_in_as(other_user)
    get root_path
    assert_match "0 trips", response.body
    other_user.trips.create!(tripinfo: "Roma-Barcelona",
    													tripdate: "2016-12-25 07:10:00",
      												maxtravellers: 4, triptype: true)
    get root_path
    assert_match /1 trip\b/, response.body
  end
end