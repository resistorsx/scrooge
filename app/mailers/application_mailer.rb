class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@carpolling.com'
  layout 'mailer'
end