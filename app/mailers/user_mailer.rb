class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end

  def trip_booked(user,trip)
    @user = user
    @trip = trip
    @driver = User.find_by(id: @trip.user_id)
    mail to: @driver.email, subject: "Your trip has been booked"
  end

  def trip_canceled(user,trip)
    @user = user
    @trip = trip
    @driver = User.find_by(id: @trip.user_id)
    mail to: @driver.email, subject: "Reservation on your trip has been canceled"
  end

  def trip_deleted(user,trip)
    @user = user
    @trip = trip
    @driver = User.find_by(id: @trip.user_id)
    mail to: @user.email, subject: "Your reserved trip has been deleted"
  end
end