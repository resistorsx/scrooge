class Trip < ApplicationRecord
	has_many :active_reservations, class_name:  "Reservation",
                                 foreign_key: "trip_id",
                                 dependent:   :destroy
  has_many :passengers, through: :active_reservations,  source: :passenger
  belongs_to :user
  default_scope -> { order(:tripdate) }
  validates :user_id, presence: true
  validates :tripinfo, presence: true, length: { maximum: 140 }
  validates :tripdate, presence: true
  validates :maxtravellers, presence: true, inclusion: { in: [ 1, 2, 3, 4 ] }
  validates :triptype, inclusion: { in: [ true, false ] }

  # Book a trip.
  def book(other_user)
    active_reservations.create(passenger_id: other_user.id) if self.maxtravellers > self.bookings
  end

  # Cancel a reservation.
  def cancel(other_user)
    active_reservations.find_by(passenger_id: other_user.id).destroy
  end

  # Returns true if the trip has been booked by the other user.
  def booked?(other_user)
    passengers.include?(other_user)
  end

  # Returns number of reservations.
  def bookings
    passengers.count
  end
end