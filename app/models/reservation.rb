class Reservation < ApplicationRecord
	belongs_to :trip, 		 class_name: "Trip"
  belongs_to :passenger, class_name: "User"
  validates :trip_id, 		 presence: true
  validates :passenger_id, presence: true
end
