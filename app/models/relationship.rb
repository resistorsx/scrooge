class Relationship < ApplicationRecord
	belongs_to :driver, 	class_name: "User"
  belongs_to :traveler, class_name: "User"
  validates :driver_id, 	presence: true
  validates :traveler_id, presence: true
end
