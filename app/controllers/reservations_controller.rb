class ReservationsController < ApplicationController
before_action :logged_in_user

  def create
    @trip = Trip.find(params[:trip_id])
    @user = current_user
    @trip.book(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
    UserMailer.trip_booked(@user,@trip).deliver_now
  end

  def destroy
    @trip = Reservation.find(params[:id]).trip
    @user = current_user
    @trip.cancel(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
    UserMailer.trip_canceled(@user,@trip).deliver_now
  end
end