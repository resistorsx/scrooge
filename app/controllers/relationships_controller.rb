class RelationshipsController < ApplicationController
	before_action :logged_in_user

  def create
    @user = User.find(params[:traveler_id])
    current_user.myfriend(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).traveler
    current_user.notfriend(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
