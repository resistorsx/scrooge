class TripsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
  	@trip = current_user.trips.build(trip_params)
    if @trip.save
      flash[:success] = "New trip created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    if @trip.passengers.any?
      @trip.passengers.each do |user|
        UserMailer.trip_deleted(user,@trip).deliver_now
      end
    end
    @trip.destroy
    flash[:success] = "Trip deleted"
    redirect_to request.referrer || root_url
  end

  private

    def trip_params
      params.require(:trip).permit(:tripinfo, :tripdate,
                    :maxtravellers, :addinfo, :triptype)
    end

    def correct_user
      @trip = current_user.trips.find_by(id: params[:id])
      redirect_to root_url if @trip.nil?
    end
end