class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.integer :trip_id
      t.integer :passenger_id

      t.timestamps
    end
    add_index :reservations, :trip_id
    add_index :reservations, :passenger_id
    add_index :reservations, [:trip_id, :passenger_id], unique: true
  end
end
