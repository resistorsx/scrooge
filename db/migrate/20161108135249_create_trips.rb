class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.string :tripinfo
      t.datetime :tripdate
      t.integer :maxtravellers
      t.boolean :triptype
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :trips, [:user_id, :tripdate]
  end
end