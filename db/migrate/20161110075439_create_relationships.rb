class CreateRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :relationships do |t|
      t.integer :driver_id
      t.integer :traveler_id

      t.timestamps
    end
    add_index :relationships, :driver_id
    add_index :relationships, :traveler_id
    add_index :relationships, [:driver_id, :traveler_id], unique: true
  end
end
