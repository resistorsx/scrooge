class AddTravelcoinsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :travelcoins, :integer, :default => 0
  end
end
