class AddAddinfoToTrips < ActiveRecord::Migration[5.0]
  def change
    add_column :trips, :addinfo, :text
  end
end
