class AddTriptypeIndexToTrips < ActiveRecord::Migration[5.0]
  def change
  	add_index :trips, :triptype
  end
end
