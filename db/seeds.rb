# Users

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar11",
             password_confirmation: "foobar11",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

# Trips

User.first.trips.create!(tripinfo: "Roma-Pagani",
                          tripdate: "2016-11-14 07:10:00",
                          maxtravellers: 4,
                          triptype: true)
User.first.trips.create!(tripinfo: "Pagani-Roma",
                          tripdate: "2016-11-17 17:00:00",
                          maxtravellers: 4,
                          triptype: true)

User.second.trips.create!(tripinfo: "Roma-Pagani",
                          tripdate: "2016-11-15 06:50:00",
                          maxtravellers: 4,
                          triptype: true)
User.second.trips.create!(tripinfo: "Pagani-Roma",
                          tripdate: "2016-11-16 17:30:00",
                          maxtravellers: 4,
                          triptype: true)

User.third.trips.create!(tripinfo: "Roma-Pagani",
                          tripdate: "2016-11-14 07:00:00",
                          maxtravellers: 4,
                          triptype: true)
User.third.trips.create!(tripinfo: "Pagani-Roma",
                          tripdate: "2016-11-18 13:00:00",
                          maxtravellers: 4,
                          triptype: true)

User.fourth.trips.create!(tripinfo: "Roma-Pagani",
                          tripdate: "2016-11-14 07:10:00",
                          maxtravellers: 4,
                          triptype: true)
User.fourth.trips.create!(tripinfo: "Pagani-Roma",
                          tripdate: "2016-11-16 17:30:00",
                          maxtravellers: 4,
                          triptype: true)

User.fifth.trips.create!(tripinfo: "Roma-Pagani",
                          tripdate: "2016-11-15 07:00:00",
                          maxtravellers: 4,
                          triptype: true)
User.fifth.trips.create!(tripinfo: "Pagani-Roma",
                          tripdate: "2016-11-16 18:00:00",
                          maxtravellers: 4,
                          triptype: true)

# Relationship
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.myfriend(followed) }
followers.each { |follower| follower.myfriend(user) }

#Reservations
Trip.second.book(user)
Trip.third.book(user)
Trip.fourth.book(user)
Trip.fifth.book(user)