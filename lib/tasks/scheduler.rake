desc "This task is called by the Heroku scheduler add-on"

task :scheduled_destroy => :environment do
  Trip.where("tripdate < :midnight", {midnight: Time.now}).destroy_all
end

task :managing_coins => :environment do

	trips = Trip.where("tripdate < :deadline", {deadline: Time.now})

	if trips.any?
		trips.each do |trip|
			driver = User.where("id = :driver_id", {driver_id: trip.user_id}).first
			driver.update(travelcoins: driver.travelcoins + trip.bookings)

			crew = trip.passengers
			if crew.any?
				crew.each do |user|
					user.update(travelcoins: user.travelcoins -= 1)
				end
			end
		end

	trips.destroy_all	
	end
end